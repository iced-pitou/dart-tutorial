// Class declaration
class Calculator {
  static int product(int a, int b) {
    return a * b;
  }

/* With null safety */
  static num? divide(int a, int b) {
    if (b == 0) {
      return null;
    }
    return a / b;
  }
}

class Basics {
  static void varConstFinal() {
    var variable = 1;
    variable = 2; // variable is reassignable
    print('Variable : ${variable.toString()}');
    const constant = 1; // constant cannot change
    print('Constant : ${constant.toString()}');
    final int fValue; // Type cannot be infered, here it is declared
    fValue = 1; // fValue can be assigned once, at declaration or later
    print('Final: ${fValue.toString()}');
  }

  static void collections() {
    print('Lists : ${['a', 'b', 'a']}'); // Shows a, b, a
    // ignore: equal_elements_in_set
    print('Sets : ${{'a', 'b', 'a'}}'); // Shows a, b (no duplicates)
    // Shows 1: a, 2: b (key-value types implied)
    print('Maps : ${{1: 'a', 2: 'b'}}');
  }
}

class Nullsafety {
  static void canBeNull(int? a) {
    if (a == null) {
      print('${a.toString()} is null');
    } else {
      print(a);
    }
  }

  static void firstNonNull(int? a, int? b) {
    final nonNull = a ?? b ?? 0;
    print(nonNull); // Will default to 0 if a and b are null
  }

  static void assignIfNotNull(int? a) {
    int? nullVar;
    nullVar ??= a;
    // nullVar remains null if a is null, otherwise takes value of a
    print(nullVar);
  }

  static void conditionalIvoke(int? a) {
    // null a may not have method toDouble
    final double doubleA = a?.toDouble() ?? 0;
    print(doubleA);
  }
}

// Declaration outiside of any class
enum AnimalType { poodle, penguin, bunny }

class Living {
  bool isAlive() {
    return true;
  }
}

class Animal extends Living {
  final String name;
  final AnimalType animalType;

  Animal(this.name, this.animalType);

  // Automatic instance
  factory Animal.pitou() {
    return Animal('Pitou', AnimalType.poodle);
  }

  String introduce() {
    return 'My name is $name, I am a ${animalType.name}';
  }

  //Overrinding comparison
  @override
  bool operator ==(covariant Animal other) => other.name == name;

  @override
  int get hashCode => name.hashCode;
}

class EnumsAndClasses {
  static void printEnum() {
    print(AnimalType);
  }

  static void switchCaseBunny() {
    // Instantiation
    final bunny = Animal('Hoppy', AnimalType.bunny);
    // Use of switch case
    switch (bunny.animalType) {
      case AnimalType.poodle:
        print('Wan wan !');
        break;
      case AnimalType.penguin:
        print('Noot noot !');
        break;
      case AnimalType.bunny:
        print('Pyon pyon !');
        break;
    }
  }

  static void introducePoodle() {
    final pitou = Animal.pitou();
    // Calling methods
    if (pitou.isAlive()) print(pitou.introduce());
  }
}

class Person {
  final String name;
  Person(this.name);
}

// Append additional methods
extension Introduce on Person {
  String introduce() {
    return 'My name is $name';
  }
}

class AdvancedDart {
  static void extensionOnPerson() {
    final miou = Person('miou');
    print(miou.introduce());
  }

  Future<int> twiceNum(int num) {
    return Future.delayed(Duration(seconds: 3), () => num * 2);
  }

  // Asynchronicity (method cannot be static)
  void futures() async {
    final num = await twiceNum(3);
    print(num);
  }

  Stream<String> pipedValue() {
    return Stream.periodic(Duration(seconds: 3), (val) => 'Hello');
  }

  // Continuous asynchronicity
  void streams() async {
    await for (final val in pipedValue()) {
      print(val);
    }
    print('Stream finished piping');
  }

  // Can be async*
  Iterable<int> yieldIntegers() sync* {
    int val = 1;
    do {
      yield val++;
    } while (val <= 10);
  }

  // Iterables with internal calculations
  void generators() {
    for (final num in yieldIntegers()) {
      print(num);
    }
  }

  // Generics, when type is inferred at runtime
  static void generics() {
    final pairString = Pair('Hello', 'World');
    final pairObj = Pair('Hello', 42);
    print(pairString);
    // Defaults to Object for unshared types, so be specific
    print(pairObj);
  }
}

class Pair<T> {
  T a;
  T b;
  Pair(this.a, this.b);
}
