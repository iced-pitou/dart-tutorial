import 'package:dart_tutorial/dart_tutorial.dart' as dart_tutorial;

void main(List<String> arguments) {
  final String helloWorld =
      'Hello world: ${dart_tutorial.Calculator.product(6, 7)}!';
  print(helloWorld);
}
